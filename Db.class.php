<?php 
/**
	 * @author Chathula Sampath <Schathula@gmail.com>
	 * @copyright 2014 || SLTUTS
	 * @license http://www.php.net/license/3_01.txt PHP License 3.01
	 * @link http://sltuts.com || http://fb.com/c.sampathperera || http://twitter.com/ChathulaC
*/

class DB {
	
	/* This method called "connect" use to connect DB || PDO METHOD */
	public static function connect($dbtype="mysql",$host="localhost",$dbname,$dbuser,$dbpass, $charset) {

		try {

			$conn = new PDO("$dbtype:host=$host;dbname=$dbname;", $dbuser, $dbpass);
			$conn->setAttribute(PDO::ATTR_ERRMODE, PDO::ERRMODE_EXCEPTION);
			$conn->setAttribute(PDO::MYSQL_ATTR_INIT_COMMAND, "SET NAMES $charset");
			$conn->setAttribute(PDO::ATTR_DEFAULT_FETCH_MODE, PDO::FETCH_OBJ);

			return $conn;

		} catch (PDOException $e) {
			die("<b>Error: </b>" . $e->getMessage());
		}

	}

	/* This method called "query" use to RUN QUERY FROM DB || PDO METHOD */
	public static function query($query, $bind_values = "", $connection, $fetch = "") {

		if (!$connection) {
			echo "Connection Error!";
			die();
		}

		$fetch = strtoupper($fetch);

		$stmt = $connection->prepare($query);
		(empty($bind_values)) ? $stmt->execute() : $stmt->execute($bind_values);
		if (empty($fetch)) {
			return $stmt;
		} else {
			switch ($fetch) {
				case "FETCH":
					$result = $stmt->fetchAll();
					return $result;
					break;

				case "FETCH_OBJ":
					$result = $stmt->fetchAll(PDO::FETCH_OBJ);
					return $result;
					break;

				case "FETCH_ASSOC":
					$result = $stmt->fetchAll(PDO::FETCH_ASSOC);
					return $result;
					break;

				case "FETCH_BOTH":
					$result = $stmt->fetchAll(PDO::FETCH_BOTH);
					return $result;
					break;

				case "FETCH_BOUND":
					$result = $stmt->fetchAll(PDO::FETCH_BOUND);
					return $result;
					break;

				case "FETCH_CLASS":
					$result = $stmt->fetchAll(PDO::FETCH_CLASS);
					return $result;
					break;

				case "FETCH_LAZY":
					$result = $stmt->fetch(PDO::FETCH_LAZY);
					return $result;
					break;

				case "FETCH_NAMED":
					$result = $stmt->fetchAll(PDO::FETCH_NAMED);
					return $result;
					break;

				case "FETCH_NUM":
					$result = $stmt->fetchAll(PDO::FETCH_NUM);
					return $result;
					break;
				
				default:
					echo "Wrong FETCH METHOD!";
					break;
			}
		}
		
	}

}

